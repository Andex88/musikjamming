import psycopg2
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

connectionString = "dbname='ahnguyen18_db' user='ahnguyen18'"
connection = psycopg2.connect(connectionString)

cursor = connection.cursor()

sqlStatement1 = "SELECT artist_lastfm, country_lastfm, listeners_lastfm, scrobbles_lastfm FROM artist WHERE artist_lastfm IS NOT NULL AND country_lastfm IS NOT NULL;"

cursor.execute(sqlStatement1)


records = cursor.fetchall()

artist_lastfm = []
country_lastfm = []
listeners_lastfm = []
scrobbles_lastfm = []

for row in records[:30]:
    artist_lastfm.append(row[0].encode('utf-8'))
    country_lastfm.append(row[1].encode('utf-8'))
    listeners_lastfm.append(row[2])
    scrobbles_lastfm.append(row[3])

fig = plt.figure(figsize = (17, 8))    
plt.bar(country_lastfm, listeners_lastfm, color ='red', width = 0.5)
plt.xlabel('Country')
plt.ylabel('Listeners')
plt.savefig("Listeners vs Countries.png")

fig = plt.figure(figsize = (17, 8)) 
plt.bar(country_lastfm, scrobbles_lastfm, color ='green', width = 0.5)
plt.xlabel('Countries')
plt.ylabel('Scrobbles')
plt.savefig("Countries vs Scrobbles.png")

fig = plt.figure(figsize = (50, 8)) 
plt.bar(artist_lastfm, listeners_lastfm, color ='blue', width = 0.5)
plt.xlabel('Artist')
plt.ylabel('Listeners')
plt.savefig("Artist vs Listeners.png")

fig = plt.figure(figsize = (50, 8)) 
plt.bar(artist_lastfm, scrobbles_lastfm, color ='yellow', width = 0.5)
plt.xlabel('Artist')
plt.ylabel('Scrobbles')
plt.savefig("Artist vs Scrobbles.png")