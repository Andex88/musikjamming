drop table if exists artist cascade;
CREATE table artist(
mbid varchar(40),
artist_mb text,
artist_lastfm text, 
country_mb text,
country_lastfm text,
tags_mb text,
tags_lastfm text,
listeners_lastfm int,
scrobbles_lastfm int,
ambigous_artists boolean
);

\COPY artist(mbid, artist_mb, artist_lastfm, country_mb, country_lastfm, tags_mb, tags_lastfm, listeners_lastfm, scrobbles_lastfm, ambigous_artists) FROM 'artists.csv' DELIMITER ',' CSV HEADER;
